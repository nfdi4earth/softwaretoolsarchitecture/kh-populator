"""" Utility functions for (geojson) geometry"""

from typing import Dict, List, Tuple, Union

from geomet import wkt
from rdflib import Literal
from kh_populator_logic.rdf import (
    GEO,
)


def bbox_to_geojson_bbox(
    bbox: Tuple[float, float, float, float]
) -> Dict[str, Union[str, List[List[List[float]]]]]:
    minx, miny, maxx, maxy = bbox
    return {
        "type": "Polygon",
        "coordinates": [
            [
                [minx, maxy],
                [maxx, maxy],
                [maxx, miny],
                [minx, miny],
                [minx, maxy],
            ]
        ],
    }


def lat_lng_to_wkt(lng: float, lat: float) -> Literal:
    point = {"type": "Point", "coordinates": [lng, lat]}
    return Literal(wkt.dumps(point, decimals=4), datatype=GEO.wktLiteral)
