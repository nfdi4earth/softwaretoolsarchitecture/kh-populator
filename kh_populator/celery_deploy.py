import logging

from celery import Celery
from celery.schedules import crontab
from kh_populator.util import Sources, config

log = logging.getLogger(__name__)

HARVESTING_DELAY = int(
    config(
        "celery",
        "harvesting_delay",
        default=60,  # 60 seconds
    )
)

mquser = config("celery", "mquser")
mqpassword = config("celery", "mqpassword")


app = Celery(
    "kh-populator",
    broker=f"pyamqp://{mquser}:{mqpassword}@localhost//",
    backend="rpc://",
    include=[
        "kh_populator.harvesting_searchers.rdamsc_searcher",
        "kh_populator.harvesting_searchers.re3data_searcher",
        "kh_populator.harvesting_searchers.lhb_searcher",
        "kh_populator.harvesting_searchers.rsd_searcher",
        "kh_populator.harvesting_searchers.zenodo_searcher",
        "kh_populator.harvesting_searchers.openedx_harvester",
        "kh_populator.pipelines.harvest_n4e_members_info",
        "kh_populator.pipelines.harvest_incub_and_pilots",
        "kh_populator.pipelines.import_from_tables",
        "kh_populator.harvesting_searchers.gdi_searcher",
        "kh_populator.harvesting_searchers.datahub_searcher",
    ],
)


app.conf.update(
    task_routes={
        "ror_consumer.upsert": {"queue": Sources.ROR},
        "rdamsc_consumer.upsert": {"queue": Sources.RDAMSC},
        "re3data_consumer.upsert": {"queue": Sources.RE3DATA},
        "wikidata_consumer.upsert": {"queue": Sources.WIKIDATA},
        "orcid_consumer.upsert": {"queue": Sources.ORCID},
        "lhb_consumer.upsert": {"queue": Sources.LHB},
        "rsd_consumer.upsert_rsd1": {"queue": Sources.RSD1},
        "rsd_consumer.upsert_rsd2": {"queue": Sources.RSD2},
        "zenodo_consumer.upsert": {"queue": Sources.ZENODO},
        "iso_gmd_consumer.upsert": {"queue": Sources.ISOGMD},
    },
)
app.conf.beat_schedule = {
    "search-and-update-rdamsc": {
        "task": "kh_populator.harvesting_searchers.rdamsc_searcher.search",
        "schedule": crontab(hour=8, minute=0),
    },
    "search-and-update-re3data": {
        "task": "kh_populator.harvesting_searchers.re3data_searcher.search",
        "schedule": crontab(hour=9, minute=0),
    },
    "search-and-update-lhb": {
        "task": "kh_populator.harvesting_searchers.lhb_searcher.search",
        "schedule": crontab(hour=10, minute=0),
    },
    "search-and-update-rsd": {
        "task": "kh_populator.harvesting_searchers.rsd_searcher.search",
        "schedule": crontab(hour=11, minute=0),
    },
    "search-and-update-zenodo": {
        "task": "kh_populator.harvesting_searchers.zenodo_searcher.search",
        "schedule": crontab(hour=12, minute=0),
    },
    "harvest-openedx": {
        "task": "kh_populator.harvesting_searchers.openedx_harvester.harvest",
        "schedule": crontab(hour=13, minute=0),
    },
    "run-harvest_n4e_members_info": {
        "task": "kh_populator.pipelines.harvest_n4e_members_info.run_as_task",
        "schedule": crontab(hour=13, minute=30),
    },
    "run-harvest_incub_and_pilots": {
        "task": "kh_populator.pipelines.harvest_incub_and_pilots.run_as_task",
        "schedule": crontab(hour=14, minute=0),
    },
    "search-and-update-gdi": {
        "task": "kh_populator.harvesting_searchers.gdi_searcher.search",
        "schedule": crontab(hour=21, minute=0, day_of_week=5),
    },
    "search-and-update-datahub": {
        "task": "kh_populator.harvesting_searchers.datahub_searcher."
        + "search",
        "schedule": crontab(hour=17, minute=30, day_of_week=5),
    },
    "update-organizations-ror": {
        "task": "kh_populator.harvesting_searchers.khresources_updater."
        + "update_organizations_ror",
        "schedule": crontab(hour=15, minute=0),
    },
    "update-organizations-wikidata": {
        "task": "kh_populator.harvesting_searchers.khresources_updater."
        + "update_organizations_wikidata",
        "schedule": crontab(hour=16, minute=0),
    },
    "update-persons-orcid": {
        "task": "kh_populator.harvesting_searchers.khresources_updater."
        + "update_persons_orcid",
        "schedule": crontab(hour=17, minute=0),
    },
}
app.conf.timezone = "Europe/Berlin"
