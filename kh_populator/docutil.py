import logging
import matplotlib.pyplot as plt

from kh_populator.celery_deploy import app

log = logging.getLogger(__name__)

colors = [
    "#8dd3c7",
    "#ffffb3",
    "#bebada",
    "#fb8072",
    "#80b1d3",
    "#fdb462",
    "#b3de69",
    "#fccde5",
    "#d9d9d9",
    "#bc80bd",
    "#ccebc5",
    "#ffed6f",
    "#eeaa66",
    "#bbfffa",
]


def chart_from_beat_schedule(filename="beat_schedule.png", grid=False):
    """
    Create a Gantt Chart from the beat schedule and save
    it to a *.png-file.
    The implementation uses the matplotlib library and is fundamentally
    based on this resource:
    https://www.geeksforgeeks.org/python-basic-gantt-chart-using-matplotlib/
    """

    chart_entries = []

    for i, _entry in enumerate(app.conf.beat_schedule):
        schedule = app.conf.beat_schedule[_entry]["schedule"]
        days = [h for h in list(schedule.day_of_week)]
        if (len(days)) == 7:
            day_vals = [(0.5, 7)]
        else:
            day_vals = [(day - 0.5, 1) for day in days]
        # get the minute and format it to 2 digits
        minute = schedule.minute.pop()
        if minute <= 9:
            minute = f"0{minute}"
        if len(schedule.minute) > 0:
            log.warning(f"More than one minute in schedule for {_entry}!")
        hour = schedule.hour.pop()
        if len(schedule.hour) > 0:
            log.warning(f"More than one hour in schedule for {_entry}!")
        chart_entries.append(
            {
                "x_axis": day_vals,
                "name": _entry,
                "color": colors[i],
                "time": f"{hour}:{minute}",
                "time_sortable": int(f"{hour}{minute}"),
            }
        )

    # sort the chart entries by time
    chart_entries = sorted(
        chart_entries,
        key=lambda x: x["time_sortable"],
    )
    # set the y-axis values
    for i, ce in enumerate(chart_entries):
        ce["y_axis"] = (i - 0.4, 0.8)

    # Declaring a figure "gnt"
    # 'figsize' attribute allows to set the width and height of the graph
    fig, gnt = plt.subplots(figsize=(10, 6))

    # Setting labels for x-axis and y-axis
    gnt.set_xlabel("Days of Week")
    gnt.set_ylabel("Harvesting Pipeline")

    # Setting Y-axis limits
    gnt.set_ylim(-1, len(chart_entries))

    # Setting ticks on x-axis
    # Setting X-axis limits
    # limit is set to 8 because we have 7 days of week
    gnt.set_xlim(0, 8)
    # range(9) is defined to have an empty tick at start and end
    gnt.set_xticks(range(9))
    # Labelling tickes of x-axis
    gnt.set_xticklabels(
        (
            [
                "",
                "Sunday",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "",
            ]
        )
    )

    # Setting ticks on y-axis
    gnt.set_yticks(range(len(app.conf.beat_schedule)))
    # Labelling tickes of y-axis
    gnt.set_yticklabels([ce["name"] for ce in chart_entries], rotation=0)
    plt.tight_layout()

    # Setting graph attribute
    gnt.grid(grid)

    # Declaring all bars in schedule
    for i, entry in enumerate(chart_entries):
        gnt.broken_barh(
            entry["x_axis"],
            entry["y_axis"],
            facecolors=(entry["color"]),
        )
        for start, duration in entry["x_axis"]:
            gnt.text(
                start + duration / 2,
                entry["y_axis"][0] + 0.4,
                entry["time"],
                ha="center",
                va="center",
                fontsize=10,
                color="black",
            )

    plt.savefig(filename)
