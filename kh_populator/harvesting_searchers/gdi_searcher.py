from celery.utils.log import get_task_logger
from owslib.csw import CatalogueServiceWeb

from kh_populator.celery_deploy import app
from kh_populator.util import Sources, GDI_SOURCE_SYSTEM
from kh_populator.harvesting_consumers.iso_gmd_consumer import (
    upsert_gmd,
)
from kh_populator.n4e_task import N4ETask
from kh_populator_domain.iso_gmd import ISO_SCHEMA_URL

log = get_task_logger(__name__)

re3dataDOI = "http://doi.org/10.17616/R3PS9B"
gdi_endpoint_url = (
    "https://mis.bkg.bund.de/csw?REQUEST=GetCapabilities&SERVICE=CSW"
)
gdi_endpoint_url = "https://gdk.gdi-de.org/gdi-de/srv/eng/csw"
gdi_frontend_url = (
    "https://gdk.gdi-de.org/gdi-de/srv/ger/catalog.search#/metadata/"
)


@app.task(base=N4ETask, bind=True)
def search(self: app.task):
    print("GDI searcher: initiating harvest")
    # Note: default is to request version 2.0.2. The owslib library currently
    # only supports 2.0.2 and 3.0.0
    # TODO: Decide: should we collect the available versions beforehand?
    csw = CatalogueServiceWeb(gdi_endpoint_url, version="2.0.2", timeout=30)
    # use_skip_caps = False
    # try:
    #    csw.getrecords2()
    # except RuntimeError:
    #    use_skip_caps = True
    # if use_skip_caps:
    #    csw = CatalogueServiceWeb(
    #        data_service_url, skip_caps=True, version="2.0.2"
    #    )
    page_size = 100
    current = 0
    has_iterated_all = False
    datasets = 0
    services = 0
    application = 0
    series = 0
    other = 0
    while not has_iterated_all:
        # currently harvest with no constraint filter, e.g.
        # query = PropertyIsEqualTo("dc:subject", "opendata")
        print(f"GDI searcher: fetch records {current} to {current+page_size}")
        csw.getrecords2(
            # constraints=[query],
            startposition=current,
            maxrecords=page_size,
            esn="full",
            # need to set outputschema to iso because default csw returns
            # metada in dublin core and does not contain urls to e.g. WMS
            # service
            outputschema=ISO_SCHEMA_URL,  # http://www.w3.org/ns/dcat#
        )
        if current == 0:
            print(
                f"GDI Harvester: Will harvest {csw.results['matches']} records"
            )
        else:
            print(f"Iterating records {current} to {current + page_size}")
        current += page_size
        if csw.results["matches"] <= current:
            has_iterated_all = True
        for record_id, record in csw.records.items():
            if record.hierarchy == "dataset":
                datasets += 1
            elif record.hierarchy == "service":
                services += 1
            elif record.hierarchy == "application":
                application += 1
            elif record.hierarchy == "series":
                series += 1
            else:
                other += 1
            if record.hierarchy in ["dataset", "service", "collection"]:
                record_serializable = {"recordxml": record.xml.decode("utf-8")}
                record_source_url = gdi_frontend_url + record_id
                upsert_gmd.apply_async(
                    [
                        record_id,
                        record_serializable,
                        GDI_SOURCE_SYSTEM,
                        record_source_url,
                    ],
                    queue=Sources.ISOGMD.name,
                )
        # for testing!
        # has_iterated_all = True
        print(
            f"GDI searcher: finished, iterated datasets: {datasets}, "
            + f"services: {services}, application: {application}, series:"
            + f" {series}, other: {other}"
        )
