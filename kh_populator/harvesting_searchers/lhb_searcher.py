"""
Module which is responsible for search and update of lhb articles from
the LHB Gitlab.
"""

from kh_populator.celery_deploy import app
from kh_populator.util import Sources, LHB_SOURCE_SYSTEM
from kh_populator.harvesting_consumers.lhb_consumer import upsert
from kh_populator.harvesting_searchers.khresources_updater import (
    get_all_harvested_ids_for_source_system,
)
from kh_populator.n4e_task import N4ETask
from kh_populator_domain import gitlab_source


@app.task(base=N4ETask, bind=True)
def search(self: app.task):
    # Query all markdown files in the Gitlab repo of the LHB
    lhb_mkdocs_files = gitlab_source.get_files_in_repo(
        gitlab_source.GITLAB_AACHEN_URL,
        gitlab_source.LHB_GITLAB_PROJECT_ID,
        path="docs",
    )
    # the sourceSystemID is the (local) file path in the Gitlab repo
    source_id_paths = []
    for file in lhb_mkdocs_files:
        # Skip the template article
        if (
            file["path"]
            == gitlab_source.LHB_SOURCE_SYSTEM_ID_PREFIX + "example.md"
        ):
            continue
        source_id_paths.append(file["path"])

    # Additionally retrieve IDs of all already harvested articles from
    # the KH to detect if there is an object in the KH which does not exist
    # anymore in the LHB source repository.
    lhb_ids_in_kh = get_all_harvested_ids_for_source_system(
        self.target_handler, LHB_SOURCE_SYSTEM
    )
    for lhb_id_path in lhb_ids_in_kh:
        if lhb_id_path not in source_id_paths:
            source_id_paths.append(lhb_id_path)

    for lhb_id_path in source_id_paths:
        print("Send to harvest: " + lhb_id_path)
        upsert.apply_async([lhb_id_path], queue=Sources.LHB.name)
