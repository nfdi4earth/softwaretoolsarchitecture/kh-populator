"""
This pipeline is responsible for
1. creating a Project instance in the KH for the NFDI4Earth consortium
2. harvesting all member organizations of the consortium from Wikidata
3. importing a CSV list with the contact persons (each identified by their
ORCID) and updating the KH accordingly
"""

import pandas as pd
from rdflib import URIRef, Literal, BNode
from n4e_kh_schema_py.n4eschema import (
    ResearchProject,
    Organization,
    Person,
    ConsortiumRole,
    Membership,
)

from kh_populator.celery_deploy import app
from kh_populator.n4e_task import N4ETask
from kh_populator.util import (
    Sources,
    ROR_SOURCE_SYSTEM,
    ORCID_SOURCE_SYSTEM,
    WIKIDATA_SOURCE_SYSTEM,
    N4E_UPLOADER_SOURCE_SYSTEM,
    get_id,
)
from kh_populator.harvesting_consumers import (
    ror_consumer,
    wikidata_consumer,
    orcid_consumer,
)

from kh_populator.targets.target_handler import TargetHandler
from kh_populator_domain import wikidata
from kh_populator_logic.rdf import (
    DFGFO,
    jsonld_dict_to_metadata_object,
)


CONSORTIUM_CONTACTS_CSV_URL = (
    "https://git.rwth-aachen.de/nfdi4earth/onestop4all"
    + "/consortiummap/-/raw/main/nfdi4earth_contacts.csv"
)


# NOTE: We actually never push any tasks to the celery queue N4E, this is
# only used to define the user and password used for the target_handler
# when this single task is executed (see definition of N4ETask for more info)
@app.task(base=N4ETask, bind=True, source_system_queue=Sources.N4E)
def run_as_task(self: app.task):
    run_pipeline(self.target_handler)


def run_pipeline(target_handler: TargetHandler) -> None:
    # ---- Step 1: create the initial Project NFDI4Earth ----
    id_ = get_id(N4E_UPLOADER_SOURCE_SYSTEM, "n4econsortium")
    project = ResearchProject(
        id=id_,
        name=Literal(
            "NFDI4Earth - NFDI Consortium Earth System Sciences", lang="en"
        ),
    )
    project.sourceSystem = N4E_UPLOADER_SOURCE_SYSTEM

    project.url = URIRef("https://nfdi4earth.de/")
    project.subjectArea.append(DFGFO["34"])
    object_ = target_handler.get_as_jsonld_frame(id_)
    if object_ is None:
        target_handler.create(project)

    # ---- Step 2: add those organizations which are part of NFDI4Earth ----
    organizations_query = wikidata.get_NFDI4Earth_organizations_query
    wikidata_response = wikidata.execute_query(
        organizations_query, "application/json"
    )
    repeat_after_async_create = False
    missing = []
    for binding in wikidata_response.json()["results"]["bindings"]:
        if "rorID" in binding:
            ror_id = binding["rorID"]["value"]
            organization_kh_iri = get_id(
                ROR_SOURCE_SYSTEM,
                ror_id,
            )
            if not target_handler.exists(organization_kh_iri):
                ror_consumer.upsert.apply_async(
                    [ror_id], queue=Sources.ROR.name
                )
                repeat_after_async_create = True
                missing.append(ror_id)
                continue
        else:
            wikidata_url = binding["institution"]["value"]
            wikidata_id = wikidata.wikidata_uri_to_id_str(wikidata_url)
            organization_kh_iri = get_id(
                WIKIDATA_SOURCE_SYSTEM,
                wikidata_id,
            )
            if not target_handler.exists(organization_kh_iri):
                wikidata_consumer.upsert.apply_async(
                    [wikidata_id], queue=Sources.WIKIDATA.name
                )
                missing.append(wikidata_id)
                repeat_after_async_create = True
                continue
        # ---- Step 3: retrieve harvested organization and add membership ----
        current_jsonld = target_handler.get_as_jsonld_frame(
            organization_kh_iri
        )
        organization = jsonld_dict_to_metadata_object(
            current_jsonld, Organization
        )
        organization.id = organization_kh_iri
        # now add membership property
        membership = Membership(
            organization=project.id,
            role=ConsortiumRole(binding["roleLabel"]["value"]),
        )
        organization.hasMembership = membership
        target_handler.update(organization)

    # Provisional workaround for LRZ which is not directly listed in the
    # Wikidata entry because officially the parent organization is the member
    ror_consumer.upsert.apply_async(["05558nw16"], queue=Sources.ROR.name)

    # ---- Step 4: get the contact CSV add/update the persons and role ----
    df = pd.read_csv(CONSORTIUM_CONTACTS_CSV_URL, sep=";")
    for _, row in df.iterrows():
        if not isinstance(row["orcid"], str):
            person = Person(id=BNode(), name=row["name"], email=row["email"])
            person_kh_iri = person
        else:
            person_kh_iri = get_id(ORCID_SOURCE_SYSTEM, row["orcid"])
            if not target_handler.exists(person_kh_iri):
                orcid_consumer.upsert.apply_async(
                    [row["orcid"]], queue=Sources.ORCID.name
                )
                missing.append(row["orcid"])
                repeat_after_async_create = True
                continue
            current_jsonld = target_handler.get_as_jsonld_frame(person_kh_iri)
            current_kh_person = jsonld_dict_to_metadata_object(
                current_jsonld, Person
            )
            # if the record harvested from ORCID did not include the email,
            # we add it to the Person object
            if len(current_kh_person.email) == 0:
                current_kh_person.email.append(Literal(row["email"]))
                target_handler.update(current_kh_person)

        # provisional workaround for entry in table which has no org_ror
        # even though ror id exists
        if (
            not isinstance(row["org_ror"], str)
            and row["wikidata_id"] == "Q65233647"
        ):
            row["org_ror"] = "03rgygs91"
        if not isinstance(row["org_ror"], str):
            org_wikidata_id = row["wikidata_id"]
            organization_kh_iri = get_id(
                WIKIDATA_SOURCE_SYSTEM,
                org_wikidata_id,
            )
        else:
            organization_kh_iri = get_id(ROR_SOURCE_SYSTEM, row["org_ror"])
        if not organization_kh_iri:
            if repeat_after_async_create:
                continue
            else:
                raise ValueError(
                    "Error: Organization in row %s does not match " % str(row)
                    + "the consortium member organizations according to "
                    + "the Wikidata instance for NFDI4Earth: Q108542504"
                )

        json_ld = target_handler.get_as_jsonld_frame(organization_kh_iri)
        if json_ld:
            orga = jsonld_dict_to_metadata_object(json_ld, Organization)
            # finally connect the contact person to their organization
            orga.hasNFDI4EarthContactPerson = person_kh_iri
            target_handler.update(orga)

    if repeat_after_async_create:
        print(
            "Metadata is being harvested from ROR and ORCID - please wait "
            + "a few minutes, then repeat this pipeline to finalize."
        )
        print("Not yet harvested:")
        print(missing)
    else:
        print("N4E members harvesting pipeline finished successfully")
