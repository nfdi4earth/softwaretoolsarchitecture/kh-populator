"""
Function which harvests incubator metadata from the NFDI4Earth gitlab
catalog
"""

from typing import Dict, Optional, Union, List
import base64
import logging
import time
import traceback

from gitlab import Gitlab
from gitlab.v4.objects.projects import Project
from jsonschema import validate
from rdflib import URIRef
import requests
import yaml

from n4e_kh_schema_py.n4eschema import (
    ResearchProject,
    Publication,
    SoftwareSourceCode,
    LHBArticle,
    Dataset,
)

from kh_populator.celery_deploy import app
from kh_populator.harvesting_consumers.publication_consumer import (
    upsert as upsert_publication,
)
from kh_populator.harvesting_consumers.zenodo_consumer import (
    upsert as upsert_zenodo,
    get_zenodo_record_iri_if_exists_in_kh,
)
from kh_populator.harvesting_consumers.lhb_consumer import (
    get_lhb_article_kh_iri,
    upsert as upsert_lhb,
)
from kh_populator.n4e_task import N4ETask
from kh_populator.targets.target_handler import TargetHandler
from kh_populator.util import (
    get_id,
    get_custom_schema,
    Sources,
    N4E_UPLOADER_SOURCE_SYSTEM,
    PUB_SOURCE_SYSTEM,
)
from kh_populator_domain import gitlab_source, zenodo
from kh_populator_logic.rdf import SCHEMA, DFGFO

log = logging.getLogger(__name__)

validator_schema = {}


def init_validator_schema():
    validator_schema.update(get_custom_schema("incubator_pilot_schema.json"))


def handle_outcome(
    outcome_dict: Dict, project: Project
) -> Union[Publication, SoftwareSourceCode]:
    if outcome_dict["type"] == "SoftwareSourceCode":
        if "citationFile" in outcome_dict:
            cff_url = outcome_dict["citationFile"]
            if cff_url.startswith("http://") or cff_url.startswith("https://"):
                metadata_str = requests.get(cff_url).text
            else:
                metadata_str = base64.b64decode(
                    project.files.get(
                        cff_url,
                        ref=project.default_branch,
                    ).content
                )
        else:
            metadata_str = base64.b64decode(
                project.files.get(
                    file_path="CITATION.cff",
                    ref=project.default_branch,
                ).content
            )
        metadata = yaml.safe_load(metadata_str)
        software = gitlab_source.cff_to_software_code(metadata)
        if software.codeRepository is None:
            if "repository-code" not in outcome_dict:
                raise ValueError(
                    "A value for repository-code must be given for each "
                    + "SoftwareSourceCode either in the CFF or in the "
                    + "nfdi4earth-meta.yaml"
                )
            else:
                software.codeRepository = outcome_dict["repository-code"]
        if "hasNotebook" in outcome_dict:
            software.hasNotebook = outcome_dict["hasNotebook"]
        if "hasContainerFile" in outcome_dict:
            software.hasContainerFile = outcome_dict["hasContainerFile"]
        if "targetProduct" in outcome_dict:
            # hasNotebook and hasContainerFile are defined as type Literal
            # with datatype=xsd:uri which is handled automatically by LinkML
            # whereas targetProduct requires an IRI, therefore use URIRef
            software.targetProduct = URIRef(outcome_dict["targetProduct"])

        # TODO: handle citation correctly in KH
        software.citation = []

        sourceSystem = N4E_UPLOADER_SOURCE_SYSTEM
        software.sourceSystem = sourceSystem
        software.sourceSystemID = software.codeRepository
        iri = get_id(sourceSystem, str(software.sourceSystemID))
        software.id = iri
        return software
    elif outcome_dict["type"] == "Publication":
        doi = outcome_dict["doi"]
        article = Publication(id="tmp", name="", identifier=doi)
        return article
    elif outcome_dict["type"] == "LHBArticle":
        article = LHBArticle(id="tmp", name="", url=outcome_dict["url"])
        return article
    elif outcome_dict["type"] == "Dataset":
        article = Dataset(id="tmp", title="", landingPage=outcome_dict["url"])
        return article
    else:
        raise ValueError(
            "Incorrect type %s in input, allowed: SoftwareSourceCode or"
            % outcome_dict["type"]
            + " Publication"
        )


def harvest_individual_research_project(
    project_id: int,
    n4e_project_iri: URIRef,
    project_type: str,
) -> Optional[ResearchProject]:
    project = Gitlab(gitlab_source.GITLAB_AACHEN_URL).projects.get(project_id)
    try:
        txt = base64.b64decode(
            project.files.get(
                file_path="nfdi4earth-meta.yaml",
                ref=project.default_branch,
            ).content
        )
    except Exception:
        log.warning(
            "Failed to harvest incubator nfdi4earth-meta.yaml,"
            + " skipping incubator %d %s" % (project.id, project.name)
        )
        traceback.print_exc()
        return None
    metadata = yaml.safe_load(txt)
    if len(validator_schema) == 0:
        init_validator_schema()
    # Check that the metadata complies to the schema
    validate(metadata, validator_schema)
    name = metadata["name"]
    research_project = ResearchProject(id="tmp", name=name)
    if "description" in metadata:
        research_project.description = metadata["description"]
    if "keywords" in metadata:
        research_project.keywords = metadata["keywords"]
    if "subjectArea" in metadata:
        for subjectAreaId in metadata["subjectArea"]:
            # TODO: consider more robust validation whether subjectArea
            # exists in dfgfo
            subjectAreaIRI = DFGFO[subjectAreaId.replace("dfgfo:", "")]
            research_project.subjectArea.append(subjectAreaIRI)
    research_project.funder = n4e_project_iri
    research_project.additionalType = project_type
    research_project.sourceSystem = N4E_UPLOADER_SOURCE_SYSTEM
    research_project.sourceSystemID = str(project.id)
    research_project.sourceSystemURL = project.web_url
    if "hasOutcome" in metadata:
        for outcome_dict in metadata["hasOutcome"]:
            # handle default value from the template, which means that
            # the metadata has not been customized yet by the project,
            # so skip
            if (
                "targetProduct" in outcome_dict
                and outcome_dict["targetProduct"]
                == "http://example.org/the-interactive-servcie"
            ):
                continue
            # skip if the default doi from the template has not been
            # changed
            if (
                "doi" in outcome_dict
                and outcome_dict["doi"] == "10.5281/zenodo.8139082"
            ):
                continue
            outcome = handle_outcome(outcome_dict, project)
            if outcome:
                research_project.hasOutcome.append(outcome)
    if "hasContributedTo" in metadata:
        for contrib_dict in metadata["hasContributedTo"]:
            # handle default value from the template, which means that
            # the metadata has not been customized yet by the project,
            # so skip
            if (
                "citationFile" in contrib_dict
                and contrib_dict["citationFile"]
                == "https://raw.githubusercontent.com/citation-file-"
                + "format/citation-file-format/main/CITATION.cff"
            ):
                continue
            contrib = handle_outcome(contrib_dict, project)
            if contrib:
                research_project.hasContributedTo.append(contrib)
    return research_project


def upsert_gitlab_project(
    project_id: int,
    target_handler: TargetHandler,
    n4e_project_iri: URIRef,
    project_type: str,
) -> None:
    research_project = harvest_individual_research_project(
        project_id, n4e_project_iri, project_type
    )
    if not research_project:
        return

    def outcomes_to_kh_iris(
        outcomes: List[Union[SoftwareSourceCode, Publication]]
    ) -> List[URIRef]:
        outcomes_iris: List[URIRef] = []
        for outcome in outcomes:
            if isinstance(outcome, Publication):
                doi = outcome.identifier
                if isinstance(doi, list) and len(doi) > 0:
                    doi = doi[0]
                if doi.startswith("10.5281/zenodo."):
                    zenodo_result = zenodo.resolve_doi_to_zenodo_id(doi)
                    if zenodo_result["status"] == 429:
                        # in this case the Zenodo API blocked because of too
                        # many requests, so try harvesting the doi late
                        time.sleep(30)
                        zenodo_result = zenodo.resolve_doi_to_zenodo_id(doi)
                    if zenodo_result["status"] == 429:
                        log.warning(f"Failed to retrieve from Zenodo: {doi}")
                        continue
                    latest_zenodo_id = zenodo_result["latest_id"]
                    iri = get_zenodo_record_iri_if_exists_in_kh(
                        latest_zenodo_id, target_handler
                    )
                    if iri is None:
                        upsert_zenodo(doi)
                        iri = get_zenodo_record_iri_if_exists_in_kh(
                            latest_zenodo_id, target_handler
                        )
                        if iri is None:
                            continue
                else:
                    iri = get_id(PUB_SOURCE_SYSTEM, doi)
                    if not target_handler.exists(iri):
                        upsert_publication(doi)
                outcomes_iris.append(iri)
            elif isinstance(outcome, SoftwareSourceCode):
                oucome_kh_iri = URIRef(outcome.id)
                if target_handler.exists(oucome_kh_iri):
                    target_handler.update(outcome)
                else:
                    target_handler.create(outcome)
                outcomes_iris.append(oucome_kh_iri)
            elif isinstance(outcome, Dataset):
                query = (
                    """
                SELECT ?s WHERE {
                    ?s <http://www.w3.org/ns/dcat#landingPage>
                    "%s"^^<http://www.w3.org/2001/XMLSchema#anyURI>
                }
                """
                    % outcome.landingPage
                )
                bindings = target_handler.execute_query(query)
                found_kh_dataset = ""
                for binding in bindings:
                    found_kh_dataset = binding["s"]["value"]
                if found_kh_dataset:
                    outcomes_iris.append(URIRef(found_kh_dataset))
            elif isinstance(outcome, LHBArticle):
                if len(outcome.url) == 1:
                    article_source_system_id = outcome.url[0].replace(
                        gitlab_source.LHB_PUBLIC_BASE_URL, ""
                    )
                    kh_iri = get_lhb_article_kh_iri(article_source_system_id)
                    if target_handler.exists(kh_iri):
                        outcomes_iris.append(kh_iri)
                    else:
                        upsert_lhb(article_source_system_id)
                        if target_handler.exists(kh_iri):
                            outcomes_iris.append(kh_iri)
        return outcomes_iris

    research_project.hasOutcome = outcomes_to_kh_iris(
        research_project.hasOutcome
    )
    research_project.hasContributedTo = outcomes_to_kh_iris(
        research_project.hasContributedTo
    )
    iri = get_id(N4E_UPLOADER_SOURCE_SYSTEM, str(project_id))
    research_project.id = iri
    if target_handler.exists(iri):
        target_handler.update(research_project)
    else:
        target_handler.create(research_project)


# NOTE: We actually never push any tasks to the celery queue N4E, this is
# only used to define the user and password used for the target_handler
# when this single task is executed (see definition of N4ETask for more info)
@app.task(base=N4ETask, bind=True, source_system_queue=Sources.N4E)
def run_as_task(self: app.task):
    run_pipeline(self.target_handler)


def run_pipeline(target_handler: TargetHandler) -> None:
    # ---- Step1.1: retrieve ID of NFDI4Earth project for linking
    project_name = "NFDI4Earth - NFDI Consortium Earth System Sciences"
    predicate_objects_rep = [
        ("schema:name", project_name, "lang_literal", "en")
    ]
    namespaces = {"schema": SCHEMA}
    n4e_project_iris = target_handler.get_iri_by_predicates_objects(
        predicate_objects_rep, ResearchProject.class_class_uri, namespaces
    )
    if len(n4e_project_iris) == 0:
        raise ValueError(
            "The NFDI4Earth ResearchProject is not created yet."
            + " Run the harvest_n4e_members_info pipeline first"
        )
    n4e_project_iri = n4e_project_iris[0]

    # ---- Step 2: iterativly harvest all source documents ----
    incubator_projects = gitlab_source.get_repos_in_group(
        gitlab_source.GITLAB_AACHEN_URL, gitlab_source.N4E_INCUBATORS_GROUP_ID
    )
    pilot_projects = gitlab_source.get_repos_in_group(
        gitlab_source.GITLAB_AACHEN_URL, gitlab_source.N4E_PILOTS_GROUP_ID
    )
    # ---- Step 3: create an object for every document and upload it
    # to the KH (Create new or Update if exists already) ----
    for project in incubator_projects:
        try:
            upsert_gitlab_project(
                project.id, target_handler, n4e_project_iri, "incubator"
            )
        except Exception:
            log.warning(
                "Failed to harvest incubator %d %s"
                % (project.id, project.name)
            )
            traceback.print_exc()
    for project in pilot_projects:
        try:
            upsert_gitlab_project(
                project.id, target_handler, n4e_project_iri, "pilot"
            )
        except Exception:
            log.warning(
                "Failed to harvest incubator %d %s"
                % (project.id, project.name)
            )
            traceback.print_exc()
