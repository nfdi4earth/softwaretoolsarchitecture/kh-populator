#!/usr/bin/env python

"""Definition of cli commands"""

import click
import importlib
import logging
import os
import datetime

from kh_populator.celery_deploy import app
from kh_populator.util import _startup, config, Sources
from kh_populator.targets.init_target import create_target_handler
from kh_populator.targets.local_rdf_file import LocalRdfFile

try:
    from kh_populator.docutil import chart_from_beat_schedule
except ImportError as e:
    chart_from_beat_schedule = None
    chart_from_beat_schedule_exception = e


log = logging.getLogger(__name__)


@click.group()
@click.option("--debug/--no-debug", "-d", is_flag=True, default=False)
@click.pass_context
def main(ctx, debug):
    """
    A unified Python package for the ingestion of metadata and
    transformation to the metadata schema of the Knowledge Hub (KH)
    within NFDI4Earth.
    """
    _startup(log_level=debug and logging.DEBUG or logging.INFO)
    ctx.ensure_object(dict)
    ctx.obj["DEBUG"] = debug


pipelines = [
    module[:-3]
    for module in os.listdir(os.path.join("kh_populator", "pipelines"))
    if module.endswith(".py")
]

search_and_update_jobs = [key for key in app.conf.beat_schedule.keys()]


@main.command()
@click.pass_context
@click.option(
    "-t",
    "--target",
    type=click.Choice(
        ["cordra", "local_rdf_file", "stdout"], case_sensitive=False
    ),
    default="cordra",
    help="""
         The target KH type. Currently either `cordra` as in the production
         instance or `local_rdf_file` which stores all the harvested data in
         a local RDF file in TriG Syntax (https://www.w3.org/TR/trig/).
         """,
)
@click.option(
    "-p",
    "--pipeline",
    type=click.Choice(
        pipelines + search_and_update_jobs,
        case_sensitive=False,
    ),
    required=True,
    help="Specify the harvesting pipeline to run.",
)
def populate_kh(_, **kw) -> None:
    """
    Run a certain pipeline to populate the Knowledge Hub. Specify the pipeline
    with the -p parameter. Specify the target output with the -t parameter.
    Note: '-t stdout' will look for an existing local rdf file that has been
    created by running with the option '-t local_rdf_file', but will not store
    any data in the rdf file and instead print all harvested data to the
    console after the pipeline has finished.
    """
    start = datetime.datetime.now()
    target_handler = create_target_handler(
        Sources.N4E, target_name=kw["target"]
    )
    pipeline = kw.pop("pipeline")
    if pipeline in pipelines:
        module = importlib.import_module("kh_populator.pipelines." + pipeline)
        module.run_pipeline(target_handler)
        if isinstance(target_handler, LocalRdfFile):
            if kw["target"] == "stdout":
                target_handler.update_namespaces()
                print(target_handler.cg.serialize())
            else:
                print(
                    "\n Harvested data stored in the file: "
                    + config(
                        "rdf", "file_name", default="knowledge_hub_local.nq"
                    )
                )
        print(
            "\nFinished populating the KnowledgeHub with pipeline: " + pipeline
        )
    else:
        job_config = app.conf.beat_schedule[pipeline]
        app.send_task(job_config["task"])
        print("Started asynchronous search and update task: " + pipeline)

    time_elapsed = datetime.datetime.now() - start
    print(
        "Elapsed time: "
        + str(datetime.timedelta(seconds=time_elapsed.seconds))
    )


@main.command()
@click.pass_context
@click.option(
    "-f",
    "--filename",
    default="celery_schedule.png",
    help="""
            The filename of the chart image to be saved.
            'celery_schedule.png' is the default.
         """,
)
@click.option(
    "-g",
    "--grid",
    default=False,
    is_flag=True,
    help="""
            If set, a grid will be displayed in the chart.
            Option is a flag and does not require a value.
         """,
)
def generate_celery_chart(_, **kw) -> None:
    """
    Visualize the scheduled harvesting tasks and save into a file.

    NOTE: requires installation of "doku" extras, e.g.
    pip install -e .[docu]
    """
    if chart_from_beat_schedule is None:
        exception_txt = """
        Exception on importing modul kh_populator.docutil.
        Did you install the required dependency matplotlib?
        Error message:
        """ + str(
            chart_from_beat_schedule_exception
        )
        raise RuntimeError(exception_txt)
    chart_from_beat_schedule(**kw)


if __name__ == "__main__":
    main(obj={})  # pylint: disable=no-value-for-parameter
