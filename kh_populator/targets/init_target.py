from rdflib import URIRef

from kh_populator.util import config, HARVESTERS_CONFIG
from kh_populator.targets.cordra import Cordra
from kh_populator.targets.local_rdf_file import LocalRdfFile
from kh_populator.targets.target_handler import TargetHandler

TARGET = config(
    "celery",
    "target",
    default="cordra",
)


def create_target_handler(source, target_name) -> TargetHandler:
    username = ""
    password = ""
    source_system_iri = URIRef("default")
    if source in HARVESTERS_CONFIG:
        username = HARVESTERS_CONFIG[source]["username"]
        password = HARVESTERS_CONFIG[source]["password"]
        source_system_iri = HARVESTERS_CONFIG[source]["sourceSystemIRI"]
    if target_name == "cordra":
        return Cordra(user=username, password=password)
    else:
        return LocalRdfFile(sourceSystem=source_system_iri)
