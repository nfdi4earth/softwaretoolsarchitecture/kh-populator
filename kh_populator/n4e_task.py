from typing import Union

from celery import Task
from kh_populator.celery_deploy import Sources

from kh_populator.targets.init_target import create_target_handler, TARGET
from kh_populator.targets.target_handler import TargetHandler


from celery.utils.log import get_task_logger

log = get_task_logger(__name__)


class N4ETask(Task):
    """
    A custom celery Task class from which all KH harvesting classes should
    inherit.

    The property source_system_queue needs to be set as argument when
    instantiating the class via decorator, e.g.:

    @app.task(base=N4ETask, bind=True, source_system_queue=Queues.RE3DATA)
    def run_task(self: app.task):
    """

    source_system_queue: Union[Sources, str] = ""
    _target_handler: TargetHandler = None

    @property
    def target_handler(self):
        if self._target_handler is None:
            self._target_handler = create_target_handler(
                self.source_system_queue, TARGET
            )
        return self._target_handler
