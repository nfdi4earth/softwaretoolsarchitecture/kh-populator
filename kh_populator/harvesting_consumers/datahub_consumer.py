from datetime import datetime
from typing import Dict

from celery.utils.log import get_task_logger
from celery.exceptions import Retry as RetryException
from rdflib import URIRef
from kh_populator.celery_deploy import app, HARVESTING_DELAY
from kh_populator.targets.target_handler import TargetHandler
from kh_populator.util import (
    Sources,
    get_id,
    DATAHUB_SOURCE_SYSTEM,
    RE3DATA_SOURCE_SYSTEM,
)
from kh_populator.n4e_task import N4ETask
from kh_populator.harvesting_consumers.re3data_consumer import (
    upsert as upsert_re3data,
)
from kh_populator_domain.datahub import datahub_record_to_n4e
from kh_populator_domain.re3data import RE3DATA_BASE_URL_UI

log = get_task_logger(__name__)

repo_exists_cache: Dict[URIRef, int] = {}
# URIRef: the KH id of a repo which exists in the KH
# int: the timestamp when this information was last cached


def check_if_repo_exists_in_kh(
    target_handler: TargetHandler, re3data_kh_id: URIRef
):
    """
    To reduce http calls to the KH cache here the repository ids
    """
    cache_expires = 7200  # 2 hours
    if re3data_kh_id in repo_exists_cache:
        timestamp_now = round(datetime.now().timestamp())
        if timestamp_now - repo_exists_cache[re3data_kh_id] < cache_expires:
            return True
    exists = target_handler.exists(re3data_kh_id)
    if exists:
        repo_exists_cache[re3data_kh_id] = round(datetime.now().timestamp())
        return True
    return False


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.DTHB)
def upsert_dataset(self: app.task, record):
    """
    A celery task which receives a harvested datahub json record,
    processes it and upserts the record into the KH
    """
    try:
        source_system_id = record["uniqueId"]
        dataset = datahub_record_to_n4e(record)
        if dataset is None:
            return

        # link dataset to repository in KH
        if "organisations" in record and record["genericType"] == "data":
            organisations = record["organisations"]
            if "provider" in organisations:
                providers = organisations["provider"]
                for provider in providers:
                    if "metadata" in provider:
                        metadata = provider["metadata"]
                        if (
                            "re3data" in metadata
                            and len(metadata["re3data"]) > 0
                        ):
                            re3data_url: str = metadata["re3data"]
                            re3data_id = re3data_url.replace(
                                RE3DATA_BASE_URL_UI, ""
                            )
                            re3data_kh_id = get_id(
                                RE3DATA_SOURCE_SYSTEM, re3data_id
                            )
                            if not check_if_repo_exists_in_kh(
                                self.target_handler, re3data_kh_id
                            ):
                                if self.request.retries < 3:
                                    upsert_re3data.apply_async(
                                        [re3data_id],
                                        queue=Sources.RE3DATA.name,
                                    )
                                    self.retry(
                                        countdown=HARVESTING_DELAY / 5,
                                    )
                                    return
                                # else: waiting for harvesting re3data repo
                                # has not succeded 2 times, continuing
                                # without setting the .inCatalog property
                            else:
                                dataset.inCatalog = re3data_kh_id

        dataset.sourceSystem = DATAHUB_SOURCE_SYSTEM
        # TODO: check how persistent are identifiers in datahub index
        dataset.sourceSystemID = source_system_id

        # TODO: make sure that in fact all datasets we can retrieve from the
        # REST endpoint https://o2a-data.de/index/rest/search are also
        # available in the UI https://earth-data.de. If not we would need to
        # set e.g. https://marine-data.de for the sourceSystemURL, however
        # based on the JSON data we cannot make this distinction (because this
        # is internally done on the datahub side only be adding additionally
        # search keywords)
        dataset.sourceSystemURL = (
            "https://earth-data.de/data?q=%22" + source_system_id + "%22"
        )
        iri = get_id(DATAHUB_SOURCE_SYSTEM, source_system_id)
        dataset.id = iri
        if self.target_handler.exists(iri):
            self.target_handler.update(dataset)
        else:
            self.target_handler.create(dataset)
    except RetryException as e:
        raise e
    except Exception as e:
        print(
            f"Datahub harvesting error for record {record['id']} "
            + f"from SourceSystem {DATAHUB_SOURCE_SYSTEM}"
        )
        raise e


def get_re3data_kh_iri(re3data_url: str) -> URIRef:
    # TODO: query SPARQL endpoint and cache in re3data_url_cache
    return URIRef("")
