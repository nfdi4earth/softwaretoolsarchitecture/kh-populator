import base64
import frontmatter
import re
from typing import Tuple, List, Dict
from celery.utils.log import get_task_logger
from gitlab import Gitlab
from gitlab.exceptions import GitlabGetError

from n4e_kh_schema_py.n4eschema import LHBArticle

from kh_populator.celery_deploy import app, HARVESTING_DELAY
from kh_populator.util import Sources, LHB_SOURCE_SYSTEM, get_id
from kh_populator.n4e_task import N4ETask
from kh_populator_domain import gitlab_source
from kh_populator_logic.rdf import jsonld_dict_to_metadata_object

logger = get_task_logger(__name__)


def get_lhb_article_kh_iri(source_system_id: str):
    return get_id(LHB_SOURCE_SYSTEM, source_system_id)


def replace_links_in_markdown(article_body: str) -> Tuple[str, List[str]]:
    harvest_first = []
    # Replace links to PDF data in the LHB git
    article_body = re.sub(
        'iframe src="../',
        'iframe src="' + gitlab_source.LHB_DATA_BASE_URL,
        article_body,
    )
    # Replace links to images in the LHB git
    article_body = re.sub(
        "]\(img/",  # noqa: W605
        "](" + gitlab_source.LHB_DATA_BASE_URL + "img/",
        article_body,
    )

    # Search for reference to other articles within the LHB git
    # if one is found, replace them with the KH Link of that article
    for res in re.findall("]\((.*?)\)", article_body):  # noqa: W605
        if (
            res.startswith("http://")
            or res.startswith("https://")
            or res.startswith("mailto:")
            or not res.endswith(".md")
        ):
            continue

        suffix = ""
        if "#" in res:
            suffix = res[res.index("#") :]  # noqa: E203
            res = res.replace(suffix, "")
        # Skip if the reference was within the same document
        if len(res) == 0:
            continue
        # Skip the dummy reference in the template article
        if res in ["article.md", "artikel.md"]:
            continue
        source_system_id = gitlab_source.LHB_SOURCE_SYSTEM_ID_PREFIX + res
        kh_iri = get_id(LHB_SOURCE_SYSTEM, source_system_id)
        if not kh_iri:
            harvest_first.append(source_system_id)
        else:
            res += suffix
            article_body = article_body.replace(f"]({res})", f"]({kh_iri})")
    return (article_body, harvest_first)


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.LHB)
def upsert(
    self: app.task, sourceID: str, dependent_from_articles: Dict[str, int] = {}
):
    """
    :param sourceID: the LHB Article file name to upsert
    :param dependent_from_articles: a dict with information on articles
        which should be harvested previously to harvesting this one.
        This serves to keep track of these articles on which this one depends
        and prevent loops if one of these articles fails to be harvested.
        Example: {"article_id": 1} -> will suspend this upsert until article_id
        has been harvested. {"article_id": 2} -> will not wait any longer and
        ignore article_id.
    :return: The IRI (=KH ID) of the resource in the KH
    """
    project = Gitlab(gitlab_source.GITLAB_AACHEN_URL).projects.get(
        gitlab_source.LHB_GITLAB_PROJECT_ID
    )
    id_ = get_id(LHB_SOURCE_SYSTEM, sourceID)
    try:
        project_file = project.files.get(sourceID, ref="main")
    except GitlabGetError:
        logger.warning("LHBArticle not found - will delete: " + sourceID)
        self.target_handler.delete(id_)
        return
    content_b64 = project_file.content
    if content_b64.startswith("77u/"):
        content_b64 = content_b64[4:]
    content = base64.b64decode(content_b64)
    content = content.replace(b"\t", b"")
    post = frontmatter.loads(content)
    metadata = post.metadata
    commit = project.commits.get(project_file.last_commit_id)
    try:
        article = gitlab_source.metadata_to_lhb_article(metadata)
    except Exception as e:
        logger.warning("Metadata conversion error for " + sourceID)
        raise e

    article.dateModified = commit.created_at
    harvest_first = []
    if "isPartOf" in metadata:
        collections = metadata["isPartOf"]
        if not isinstance(collections, list):
            collections = [collections]
        for collection in collections:
            if not collection:
                continue
            source_system_id = (
                gitlab_source.LHB_SOURCE_SYSTEM_ID_PREFIX + collection
            )
            # prevent accidental infinite loop
            if source_system_id == sourceID:
                continue
            kh_iri = get_id(LHB_SOURCE_SYSTEM, source_system_id)
            exists = self.target_handler.exists(kh_iri)
            if not exists:
                if not source_system_id.endswith(".md"):
                    print(
                        "Skipping probably erronous link to collection"
                        + f" article: {source_system_id}"
                    )
                    continue
                if (
                    source_system_id
                    == gitlab_source.LHB_SOURCE_SYSTEM_ID_PREFIX
                    + "Collection1.md"
                    or source_system_id
                    == gitlab_source.LHB_SOURCE_SYSTEM_ID_PREFIX
                    + "Collection2.md"
                ):
                    continue
                harvest_first.append(source_system_id)
            else:
                article.isPartOf.append(kh_iri)

    article_body = post.content
    article_body_replaced, harvest_first_ = replace_links_in_markdown(
        article_body
    )
    harvest_first.extend(harvest_first_)
    harvest_first_cleaned = []
    # the following is important to prevent an infinite loop in case that
    # the harvest_first article does not exist or exits with error during
    # harvesting.
    # We keep track of harvest_first articles on which we are waiting for
    # and allow up to 2 retries. If dependent_from_articles[article_id] > 2
    # then we ignore this and continue
    for article_id in harvest_first:
        if (
            article_id not in dependent_from_articles
            or dependent_from_articles[article_id] == 1
        ):
            if article_id not in dependent_from_articles:
                dependent_from_articles[article_id] = 1
            else:
                dependent_from_articles[article_id] += 1
            harvest_first_cleaned.append(article_id)
            upsert.apply_async([article_id], queue=Sources.LHB.name)
        # else pass

    # make sure that the article gets harvested again later
    if len(harvest_first_cleaned) > 0:
        upsert.apply_async(
            [sourceID, dependent_from_articles],
            queue=Sources.LHB.name,
            countdown=HARVESTING_DELAY * 2,
        )
        return

    article.articleBody = article_body_replaced
    article.sourceSystemID = sourceID
    article.sourceSystem = LHB_SOURCE_SYSTEM
    article.id = id_
    current_jsonld = self.target_handler.get_as_jsonld_frame(id_)
    if current_jsonld:
        # Keep information that was added from another LHB article
        current_article = jsonld_dict_to_metadata_object(
            current_jsonld, LHBArticle
        )
        article.hasPart = current_article.hasPart
        self.target_handler.update(article)
    else:
        self.target_handler.create(article)
    for collection_article in article.isPartOf:
        current_jsonld = self.target_handler.get_as_jsonld_frame(
            collection_article
        )
        collection_article = jsonld_dict_to_metadata_object(
            current_jsonld, LHBArticle
        )
        if id_ not in collection_article.hasPart:
            collection_article.hasPart.append(id_)
            self.target_handler.update(collection_article)
