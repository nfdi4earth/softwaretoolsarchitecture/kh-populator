from io import BytesIO
from owslib.etree import etree
from owslib.iso import MD_Metadata
from celery.utils.log import get_task_logger
from rdflib import URIRef

from kh_populator.celery_deploy import app
from kh_populator.util import Sources, get_id, GDI_SOURCE_SYSTEM
from kh_populator.n4e_task import N4ETask
from kh_populator_domain.iso_gmd import csw_record_to_n4e

geoportal_url = "https://geoportal.de/Info/"

log = get_task_logger(__name__)


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.ISOGMD)
def upsert_gmd(
    self: app.task,
    record_id: str,
    csw_record_serialized: dict,
    source_system_iri: URIRef,
    source_system_url: str,
):
    """ """
    try:
        source_system_iri = URIRef(source_system_iri)
        record_bytes = bytes(
            csw_record_serialized["recordxml"], encoding="utf-8"
        )
        csw_record = MD_Metadata(etree.parse(BytesIO(record_bytes)))
        resource = csw_record_to_n4e(csw_record)
        if not resource:
            raise ValueError("Could not parse ISO-GMD record: " + record_id)
        if not resource.landingPage and source_system_iri == GDI_SOURCE_SYSTEM:
            # NOTE: Many datasets in the GDI-DE are being harvested through a
            # chain of intermediate harvesters. Unfortunately the
            # dcat:landingPage property is often not given, only in the
            # distribution.accessURL there are some links to the original data
            # providing system, however these point to the data itself and not
            # to the dataset entity in any kind dataportal/ geoportal.
            # Therefore as a temporal solution, use the Geoportal.de link for
            # the landingPage property
            resource.landingPage = geoportal_url + record_id
        resource.sourceSystem = source_system_iri
        resource.sourceSystemID = record_id
        if source_system_url:
            resource.sourceSystemURL = source_system_url
        for i, orga in enumerate(resource.publisher):
            try:
                orga_iri = self.target_handler.find_organization_by_name(
                    orga.name
                )
                if orga_iri:
                    resource.publisher[i] = orga_iri
            except Exception:
                pass

        iri = get_id(source_system_iri, record_id)
        resource.id = iri
        if self.target_handler.exists(iri):
            self.target_handler.update(resource)
        else:
            self.target_handler.create(resource)
    except Exception as e:
        print(
            f"ISO GMD harvesting error for record {record_id} "
            + f"from SourceSystem {source_system_iri}"
        )
        raise e
