import requests
from celery.utils.log import get_task_logger

from kh_populator.celery_deploy import app, HARVESTING_DELAY
from kh_populator.util import RDA_MSC_SOURCE_SYSTEM, Sources, get_id
from kh_populator.n4e_task import N4ETask
from kh_populator_domain.rdamsc import (
    create_standard_from_dict,
    RDA_MSC_BASE_URL,
    RDA_MSC_FRONTEND_URL,
)


logger = get_task_logger(__name__)


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.RDAMSC)
def upsert(self: app.task, sourceID: str):
    """
    :param sourceID: the RDAMSC ID of the standard to upsert
    :return: The IRI (=KH ID) of the resource in the KH
    """
    harvested_url = RDA_MSC_BASE_URL + sourceID.replace("msc:", "")
    response = requests.get(harvested_url, timeout=10)
    if response.status_code != 200:
        raise ValueError(
            "Request %s returned %d" % (harvested_url, response.status_code)
        )
    # TODO: hanndle timeout error as new push to queue with failed count
    rdamsc_standard_dict = response.json()["data"]
    standard = create_standard_from_dict(rdamsc_standard_dict)
    standard.sourceSystemID = sourceID
    standard.sourceSystem = RDA_MSC_SOURCE_SYSTEM
    standard.sourceSystemURL = RDA_MSC_FRONTEND_URL + sourceID.replace(
        ":", "/"
    )

    kh_iri = get_id(
        RDA_MSC_SOURCE_SYSTEM,
        sourceID,
    )
    standard.id = kh_iri
    object_ = self.target_handler.get_as_jsonld_frame(kh_iri)
    if object_ is None:
        kh_iri = self.target_handler.create(standard)
    else:
        # TODO: check that information from other harvesters
        # (e.g. DCC: owl:sameAs) is not overwritten
        # standard = jsonld_dict_to_metadata_object(data_dict,MetadataStandard)
        # kh_iri = self.target_handler.update(standard)
        pass

    if "relatedEntities" in rdamsc_standard_dict:
        for related_entity in rdamsc_standard_dict["relatedEntities"]:
            if related_entity["role"] in ["parent scheme", "child scheme"]:
                related_kh_iri = get_id(
                    RDA_MSC_SOURCE_SYSTEM,
                    related_entity["id"],
                )
                exists = self.target_handler.exists(related_kh_iri)
                if not exists:
                    upsert.apply_async(
                        [related_entity["id"]], queue=Sources.RDAMSC.name
                    )
                    upsert.apply_async(
                        [sourceID],
                        queue=Sources.RDAMSC.name,
                        countdown=HARVESTING_DELAY,
                    )
                    return
                if (
                    related_entity["role"] == "parent scheme"
                    and related_kh_iri not in standard.hasParentStandard
                ):
                    standard.hasParentStandard.append(related_kh_iri)
                elif (
                    related_entity["role"] == "child scheme"
                    and related_kh_iri not in standard.hasChildStandard
                ):
                    standard.hasChildStandard.append(related_kh_iri)

        self.target_handler.update(standard)
