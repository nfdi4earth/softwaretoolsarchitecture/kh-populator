"""
Function which harvests publications from zenodo.org uploaded by N4E

To handle different Zenodo resource versions, we use the "conceptdoi" as
sourceSystemId and during harvesting always resolve the conceptdoi to make
sure we harvest the latest version of a Zenodo resource.
"""

from typing import Optional

from kh_populator.celery_deploy import app, HARVESTING_DELAY
from kh_populator.targets.target_handler import TargetHandler
from kh_populator.util import Sources, ZENODO_SOURCE_SYSTEM, get_id
from kh_populator.n4e_task import N4ETask
from kh_populator_domain import zenodo

ZENODO_DOI_PREFIX = "10.5281/"
ZENODO_OAI_PREFIX = "oai:zenodo.org:"


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.ZENODO)
def upsert(self: app.task, doi: str):
    """"""
    zenodo_result = zenodo.resolve_doi_to_zenodo_id(doi)
    if zenodo_result["status"] == 429:
        # in this case the Zenodo API blocked because of too many requests, so
        # try harvesting the doi later
        upsert.apply_async(
            [doi], queue=Sources.ZENODO.name, countdown=HARVESTING_DELAY
        )
        return
    oai_id = ZENODO_OAI_PREFIX + zenodo_result["latest_id"]
    oai_record = zenodo.get_oai_record(oai_id)
    publication = zenodo.zenodo_object_to_publication(oai_record)
    concept_doi = zenodo.get_concept_doi_for_record(oai_record)
    publication.sourceSystemID = concept_doi
    publication.sourceSystem = ZENODO_SOURCE_SYSTEM
    publication.sourceSystemURL = "https://doi.org/" + doi
    kh_iri = get_id(ZENODO_SOURCE_SYSTEM, concept_doi)
    publication.id = kh_iri
    if self.target_handler.exists(kh_iri):
        self.target_handler.update(publication)
    else:
        self.target_handler.create(publication)


def get_zenodo_record_iri_if_exists_in_kh(
    latest_zenodo_id: str, target_handler: TargetHandler
) -> Optional[str]:
    """
    Resolves the doi of a Zenodo record to its concept doi, then checks
    whether the record is already harvested into the KH (based on the
    concept doi).
    """
    oai_id = ZENODO_OAI_PREFIX + latest_zenodo_id
    oai_record = zenodo.get_oai_record(oai_id)
    concept_doi = zenodo.get_concept_doi_for_record(oai_record)
    kh_iri = get_id(ZENODO_SOURCE_SYSTEM, concept_doi)
    if target_handler.exists(kh_iri):
        return kh_iri
    else:
        return None
