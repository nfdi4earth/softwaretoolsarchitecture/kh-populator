def test_help_text(script_runner):
    ret = script_runner.run(["kh_populator", "--help"])
    assert ret.success, "process should return success"
    assert ret.stderr == "", "stderr should be empty"
    assert (
        "kh_populator [OPTIONS] COMMAND" in ret.stdout
    ), "usage instructions are printed to console"
    assert (
        "NFDI4Earth" in ret.stdout
    ), "usage instructions are printed to console"
    assert (
        "populate-kh" in ret.stdout
    ), "usage instructions are printed to console"


def test_help_text_command(script_runner):
    ret = script_runner.run(["kh_populator", "populate-kh", "--help"])
    assert ret.success, "process should return success"
    assert ret.stderr == "", "stderr should be empty"
    assert (
        "kh_populator populate-kh [OPTIONS]" in ret.stdout
    ), "usage instructions are printed to console"
    assert (
        "Run a certain pipeline" in ret.stdout
    ), "usage instructions are printed to console"
    assert (
        "-p, --pipeline" in ret.stdout
    ), "usage instructions are printed to console"
