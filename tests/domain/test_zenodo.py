from datetime import date
from rdflib import URIRef
from n4e_kh_schema_py.n4eschema import Publication, Person, Organization
from kh_populator_domain import zenodo


def test_resolve_doi_to_zenodo_id():
    test_zenodo_doi_versioned = "10.5281/zenodo.7897514"
    zenodo_res = zenodo.resolve_doi_to_zenodo_id(test_zenodo_doi_versioned)
    assert zenodo_res == {"status": 200, "latest_id": "7897514"}


def test_zenodo_object_to_publication():
    test_record_id = "oai:zenodo.org:8344901"
    oai_record = zenodo.get_oai_record(test_record_id)
    publication = zenodo.zenodo_object_to_publication(oai_record)
    publication_test = Publication(
        id="",
        name="First draft of resource information types (protocols, vocabularies, metadata schemes) in use in NFDI4Earth (NFDI4Earth Deliverable D3.2.1)",
        description=[
            "This deliverable contains a detailed description of protocols and\nmetadata schemes which are relevant for the NFDI4Earth. Protocols\nprovide technological interoperability between Earth System Science\n(ESS) research data infrastructures, and common metadata schemes and\nvocabularies add semantic interoperability on top. The goal of this\ndeliverable is on the one hand to provide a compact overview of these\nresource information types; compact information which afterwards shall\nbe represented in the NFDI4Earth Knowledge Hub (KH). On the other hand,\nthe deliverable describes how interoperability between the KH itself and\nESS infrastructures can be reached by using established protocols for\nmetadata harvesting and ensuring that the metadata schema of the KH is\ncompatible with widely used vocabularies and schemas.",
            "This work has been funded by the German Research Foundation (DFG) through the project NFDI4Earth (DFG project no.460036893, https://www.nfdi4earth.de/) within the German National Research Data Infrastructure (NFDI, https://www.nfdi.de/).",
        ],
        url=[],
        audience=[],
        author=[
            Person(
                name=["Grieb, Jonas"],
                id="tmp",
                affiliation=[
                    Organization(
                        id="tmp",
                        name=["NFDI4Earth"],
                    )
                ],
                orcidId="0000-0002-8876-1722",
            ),
            Person(
                name=["Weiland, Claus"],
                id="tmp",
                affiliation=[
                    Organization(
                        id="tmp",
                        name=["NFDI4Earth"],
                    )
                ],
                orcidId="0000-0003-0351-6523",
            ),
            Person(
                name=["Gey, Ronny"],
                id="tmp",
                affiliation=[
                    Organization(
                        id="tmp",
                        name=["NFDI4Earth"],
                    )
                ],
                orcidId="0000-0003-1028-1670",
            ),
            Person(
                name="Stocker, Markus",
                id="tmp",
                affiliation=[
                    Organization(
                        id="tmp",
                        name=["NFDI4Earth"],
                    )
                ],
                orcidId="0000-0001-5492-3212",
            ),
            Person(
                name=["Degbelo, Auriol"],
                id="tmp",
                affiliation=[
                    Organization(
                        id="tmp",
                        name=["NFDI4Earth"],
                    )
                ],
                orcidId="0000-0001-5087-8776",
            ),
            Person(
                name=["Henzen, Christin"],
                id="tmp",
                affiliation=[
                    Organization(
                        id="tmp",
                        name=["NFDI4Earth"],
                    )
                ],
                orcidId="0000-0002-5181-4368",
            ),
            Person(
                name=["Thießen, Freya"],
                id="tmp",
                affiliation=[
                    Organization(
                        id="tmp",
                        name=["NFDI4Earth"],
                    )
                ],
                orcidId="0009-0000-2513-6543",
            ),
        ],
        keywords=[
            "NFDI",
            "NFDI4Earth",
            "NFDI4Earth Deliverable",
            "Knowledge Hub",
            "NFDI4Earth Task Area 3",
            "NFDI4Earth 2Interoperate",
        ],
    )
    publication_test.datePublished = date(2023, 9, 14)
    publication_test.identifier = "10.5281/zenodo.8344901"
    publication_test.inLanguage = URIRef(
        "http://publications.europa.eu/resource/authority/language/ENG"
    )
    publication_test.license = URIRef("http://spdx.org/licenses/CC-BY-4.0")
    for person in publication.author:
        person.id = "tmp"
        for organization in person.affiliation:
            organization.id = "tmp"

    assert publication_test == publication
