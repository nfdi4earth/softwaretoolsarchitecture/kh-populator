from n4e_kh_schema_py.n4eschema import Publication, Organization, Person

from kh_populator_domain.doi import (
    harvest_doi_metadata_via_content_negotiation,
)


def test_harvest_doi_metadata_via_content_negotiation():
    test_doi = "10.1080/17538947.2024.2391033"
    publication = harvest_doi_metadata_via_content_negotiation(test_doi)
    publication_test = Publication(
        id="tmp",
        name=[
            "GeoFRESH – an online platform for freshwater geospatial data processing"
        ],
        url=["https://doi.org/10.1080/17538947.2024.2391033"],
        author=[
            Person(id="tmp", name=["Sami Domisch"]),
            Person(id="tmp", name=["Vanessa Bremerich"]),
            Person(id="tmp", name=["Merret Buurman"]),
            Person(id="tmp", name=["Béla Kaminke"]),
            Person(id="tmp", name=["Thomas Tomiczek"]),
            Person(id="tmp", name=["Yusdiel Torres-Cambas"]),
            Person(id="tmp", name=["Afroditi Grigoropoulou"]),
            Person(id="tmp", name=["Jaime R. Garcia Marquez"]),
            Person(id="tmp", name=["Giuseppe Amatulli"]),
            Person(id="tmp", name=["Hans-Peter Grossart"]),
            Person(id="tmp", name=["Mark O. Gessner"]),
            Person(id="tmp", name=["Thomas Mehner"]),
            Person(id="tmp", name=["Rita Adrian"]),
            Person(id="tmp", name=["Luc De Meester"]),
        ],
        sameAs="https://doi.org/10.1080/17538947.2024.2391033",
        identifier="10.1080/17538947.2024.2391033",
        datePublished="2024-8-19",
    )
    publication_test.publisher = Organization(
        id="tmp", name="International Journal of Digital Earth"
    )
    publication.publisher.id = "tmp"
    for author in publication.author:
        author.id = "tmp"
    assert publication == publication_test
