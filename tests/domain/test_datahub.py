import requests
from rdflib import URIRef
from n4e_kh_schema_py.n4eschema import (
    Dataset,
    Distribution,
    Location,
    Person,
    PeriodOfTime,
    Service,
    SoftwareSourceCode,
)
from kh_populator_domain.datahub import datahub_record_to_n4e
from kh_populator.harvesting_searchers.datahub_searcher import (
    BASE_URL,
    SEARCH_QUERY,
)


def get_datahub_record_response(record_id: str):
    SEARCH_QUERY["query"] = f'id: "{record_id}"'
    response = requests.post(BASE_URL, json=SEARCH_QUERY, timeout=10)
    if response.status_code != 200:
        error_msg = (
            "Datahub search request returned response "
            + f"{response.status_code}: {response.text}"
        )
        raise ValueError(error_msg)
    return response


def test_datahub_dataset_to_n4e():
    test_record_id = "oai:pangaea.de:doi:10.1594/PANGAEA.218176"
    response = get_datahub_record_response(test_record_id)
    assert response.json()["totalHits"] == 1
    record = response.json()["records"][0]

    dataset = datahub_record_to_n4e(record)
    dataset_test = Dataset(
        id="tmp",
        title="Weather observations during BELGICA cruise BG9506",
        distribution=[
            Distribution(
                title="BG9506_weather",
                accessURL="https://doi.pangaea.de/10.1594/PANGAEA.218176?format=textfile",
            )
        ],
        spatialCoverage=Location(
            geometry="POLYGON ((-10.4013 51.4347, -10.4013 43.4973, 3.2 43.4973, 3.2 51.4347, -10.4013 51.4347))"
        ),
        creator=[
            Person(
                id="tmp",
                name="OMEX Project Members",
            )
        ],
        temporal=PeriodOfTime(
            startDate="1995-03-03T09:13:00", endDate="1995-03-17T08:35:00"
        ),
        issued="2004-11-05",
        variableMeasured=[
            "DATE/TIME",
            "LATITUDE",
            "LONGITUDE",
            "ALTITUDE",
            "Wind direction",
            "Wind speed",
            "Pressure, atmospheric",
            "Temperature, air",
            "Radiance, total",
        ],
        sameAs=[URIRef("https://doi.org/10.1594/PANGAEA.218176")],
        landingPage="https://doi.org/10.1594/PANGAEA.218176",
        publisher=[
            URIRef("http://localhost:8080/objects/n4e/ror-032e6b942"),
            URIRef("http://localhost:8080/objects/n4e/ror-04ers2y35"),
        ],
        originalDataSource="PANGAEA - Data Publisher for Earth and Environmental Science",
        license=URIRef("http://spdx.org/licenses/CC-BY-3.0"),
    )

    for person in dataset.creator:
        person.id = "tmp"
    assert dataset_test == dataset


def test_datahub_service_to_n4e():
    test_record_id = "blablador"
    response = get_datahub_record_response(test_record_id)
    assert response.json()["totalHits"] == 1
    record = response.json()["records"][0]

    service = datahub_record_to_n4e(record)
    service_test = Service(
        id="tmp",
        name=["Blablador"],
        keywords="Productivity",
        url=[
            "https://helmholtz.cloud/services/?serviceID=d7d5c597-a2f6-4bd1-b71e-4d6499d98570"
        ],
    )
    service_test.description = "Blablador is an evaluation server for Large Language Models (LLMs). Users can access a chat interface and an API to get hands-on experience with LLMs. In its current state, it is not ready for production use, but great for experiments."
    assert service_test == service


def test_datahub_software_to_n4e():
    test_record_id = "10.35089/WDCC/TRANSCLIM_V01_CHEM-CL_RESPONSE"
    response = get_datahub_record_response(test_record_id)
    assert response.json()["totalHits"] == 1
    record = response.json()["records"][0]

    software = datahub_record_to_n4e(record)
    software_test = SoftwareSourceCode(
        id="tmp",
        name=[
            "Assessing the climate effect of mitigation strategies for road traffic: The chemistry-climate response model TransClim."
        ],
        author=[
            Person(id="tmp", name="Vanessa Rieger"),
            Person(id="tmp", name="Volker Grewe"),
        ],
        license=URIRef("http://spdx.org/licenses/CC-BY-4.0"),
        url="https://dx.doi.org/10.35089/WDCC/TRANSCLIM_V01_CHEM-CL_RESPONSE",
    )
    software_test.description = (
        "The data contains the code of TransClim: " + "written in Python 2."
    )
    for author in software.author:
        author.id = "tmp"
    assert software_test == software
