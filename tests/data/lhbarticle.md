---
name: Dokumentation des NFDI4Earth Living Handbook
description: Collection der Artikel über das NFDI4Earth Living Handbook.

author:
  - name: Thomas Rose
    orcidId: http://orcid.org/0000-0002-8186-3566

inLanguage: DEU

additionalType: collection

isPartOf:
  - index.md

subjectArea:
  - unesco:mt2.35
  - unesco:concept8079
  - unesco:concept503

keywords:
  - NFDI4Earth
  - Living Handbook

audience:
  - general public

version: 1.0

license: CC-BY-4.0
---

# Dokumentation des NFDI4Earth Living Handbook

Um die Inhalte des [NFDI4Earth Living Handbook](index.md) zu pflegen, wurde ein Redaktionsteam gegründet. Dessen Hauptaufgabe ist die Unterstützung von Artikel verfassenden Personen bei der Publikation ihrer Artikel und die inhaltliche Weiterentwicklung des Living Handbook.

Diese Artikelsammlung beinhaltet Richtlinien und Hilfestellungen für die Artikel verfassenden Personen, dokumentiert den editorischen Prozess und Features des NFDI4Earth Living Handbook sowie vom Redaktionsteams getroffene Entscheidungen.
