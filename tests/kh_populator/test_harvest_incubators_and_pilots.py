from rdflib import URIRef

from n4e_kh_schema_py.n4eschema import (
    ResearchProject,
    Publication,
    LHBArticle,
    SoftwareSourceCode,
)

from kh_populator.pipelines.harvest_incub_and_pilots import (
    harvest_individual_research_project,
)


def test_harvest_individual_research_project():
    research_project = harvest_individual_research_project(
        88838, "http://localhost:8080/objects/n4e/n4e-n4econsortium", "pilot"
    )
    research_project.id = "tmp"
    research_project_test = ResearchProject(
        id="tmp",
        name=["Getting freshwater spatiotemporal data on track"],
        sourceSystemID="88838",
        sourceSystemURL="https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/pilots/GeoFRESH",
    )
    research_project_test.additionalType = "pilot"
    research_project_test.funder = (
        "http://localhost:8080/objects/n4e/n4e-n4econsortium"
    )
    research_project_test.hasOutcome = [
        Publication(id="tmp", name=[""], identifier="10.5281/zenodo.7888389"),
        Publication(
            id="tmp",
            name=[""],
            identifier=["10.1080/17538947.2024.2391033"],
        ),
        LHBArticle(
            id="tmp",
            name=[""],
            url="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/blob/main/docs/Pilot_GeoFRESH.md",
        ),
        LHBArticle(
            name=[""],
            url=[
                "https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/blob/main/docs/ShowCase_GeoFRESH.md"
            ],
            id="tmp",
        ),
    ]
    research_project_test.hasContributedTo = [
        SoftwareSourceCode(
            id="tmp",
            name="GeoFRESH",
            description="GeoFRESH (geofresh.org) is a platform that helps freshwater researchers to process point data across the global river network by providing a set of spatial tools.",
            keywords=[
                "freshwater ecosystems",
                "spatiotemporal data",
                "geospatial analysis",
                "NFDI4Earth",
            ],
            sourceSystemID="https://github.com/glowabio/geofresh",
            codeRepository="https://github.com/glowabio/geofresh",
        )
    ]
    research_project_test.hasContributedTo[0].id = URIRef(
        "http://localhost:8080/objects/n4e/n4e-https---github.com-glowabio-geofresh"
    )
    research_project_test.hasContributedTo[0].license = URIRef(
        "http://spdx.org/licenses/GPL-3.0-only"
    )
    research_project_test.hasContributedTo[0].sourceSystem = URIRef(
        "http://localhost:8080/objects/n4ekh/0550c8e5b1a0caddf73a"
    )
    research_project_test.sourceSystem = URIRef(
        "http://localhost:8080/objects/n4ekh/0550c8e5b1a0caddf73a"
    )
    assert research_project == research_project_test
