"""
Script for easy testing and printing to stdout of the OpenEdX harvesting.
Run for development/ testing as 'python test_harvest_openedx.py'

Run actual unit test as (from root of package directory)
'pytest tests/kh_populator/test_harvest_openedx.py'
"""

import json
from rdflib import URIRef
from n4e_kh_schema_py.n4eschema import (
    Person,
    LearningResource,
    EducationalLevel,
    LearningResourceType,
)
from kh_populator.harvesting_searchers.openedx_harvester import (
    get_token_headers,
    get_courses,
    api_object_to_learning_resource,
    request,
)


def mock_openedx_harvesting():
    headers = get_token_headers()
    courses = get_courses(headers)
    for course in courses:
        # TODO: below should be transformed into a unit test with one concrete
        # learning_resource example
        block = {}
        response_blocks = request(
            url_sub="api/courses/v1/courses/%s/blocks" % course["id"],
            headers=headers,
        )
        if response_blocks.ok:
            block = response_blocks.json()
        learning_resource = api_object_to_learning_resource(course, block)
        print(learning_resource)


def test_api_object_to_learning_resource():
    with open("tests/data/openedx_api_resource.json") as file:
        course = json.loads(file.read())
    with open("tests/data/openedx_api_resource_block.json") as file:
        block = json.loads(file.read())
    learning_resource = api_object_to_learning_resource(course, block)
    for author in learning_resource.author:
        author.id = "tmp"
    l_r = LearningResource(
        name=["How to create publishable netcdf-data"],
        additionalType=[],
        description=[
            "The course deals with creating publishable netCDF (Network Common Data Format) data from the Earth System Sciences (ESS). Data Centers have certain demands regarding the content and format of ESS data - we will focus in this course on creating CF (Climate and Forecast) compliant netCDF files."
        ],
        url=[
            "https://edutrain.nfdi4earth.de/courses/course-v1:NFDI4Earth+20230502CP+self-paced"
        ],
        id="tmp",
        about=[],
        audience=[],
        author=[
            Person(
                name=["Remon Sadikni"],
                id="tmp",
                url=URIRef("https://orcid.org/0009-0009-9872-021X"),
                orcidId="0009-0009-9872-021X",
            ),
            Person(
                name=["Christopher Purr"],
                id="tmp",
                url=URIRef("https://orcid.org/0000-0002-9376-432X"),
                orcidId="0000-0002-9376-432X",
            ),
        ],
        citation=[],
        contactPoint=[],
        contributor=[],
        copyrightNotice=None,
        dateModified=None,
        datePublished="2023-07-15",
        funder=[],
        hasPart=[],
        identifier=["course-v1:NFDI4Earth+20230502CP+self-paced"],
        inLanguage=[
            URIRef(
                "http://publications.europa.eu/resource/authority/language/"
                + "ENG"
            )
        ],
        isPartOf=[],
        keywords=[
            "education",
            "training",
            "tutorial",
            "netCDF",
            "CF conventions",
            "data publishing",
            "Python",
            "R",
        ],
        license=[URIRef("http://spdx.org/licenses/CC-BY-4.0")],
        publisher=[],
        subjectArea=[URIRef("https://github.com/tibonto/dfgfo/34")],
        version=None,
        altLabel=[],
        sameAs=[],
        sourceSystem=None,
        sourceSystemID="course-v1:NFDI4Earth+20230502CP+self-paced",
        sourceSystemURL="https://edutrain.nfdi4earth.de/courses/course-v1:NFDI4Earth+20230502CP+self-paced",
        competencyRequired=[
            "Prerequisites are programming skills in Python. And some experience with ESS data."
        ],
        hasNotebook=[],
        educationalLevel=[
            EducationalLevel("Intro"),
            EducationalLevel("Intermediate"),
        ],
        learningResourceType=[LearningResourceType("narrative text")],
    )
    assert l_r == learning_resource


if __name__ == "__main__":
    mock_openedx_harvesting()
