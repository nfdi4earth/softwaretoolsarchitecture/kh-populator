"""

"""

import requests
from rdflib import BNode

from n4e_kh_schema_py.n4eschema import Publication, Organization, Person

DOI_RESOLVE_BASE = "https://doi.org/"


def harvest_doi_metadata_via_content_negotiation(doi: str) -> Publication:
    """
    NOTE: Based on https://citation.crosscite.org/docs.html we decide to
    harvest using the vnd.citationstyles.csl+json as it is supported by all
    Crossref, DataCite and mEDRA. They also all support RDF, but since this
    is just a format and not a standard it is not guaranteed that the serve
    the triples according to the same schema.
    """
    url = DOI_RESOLVE_BASE + doi
    response = requests.get(
        url,
        headers={"accept": "application/vnd.citationstyles.csl+json"},
    )
    data = response.json()
    pub = Publication(
        id="tmp", name=data["title"], url=url, sameAs=url, identifier=doi
    )
    if "container-title" in data:
        pub.publisher = Organization(id=BNode(), name=data["container-title"])
    if "author" in data:
        authors = []
        first_author = None
        for author_dict in data["author"]:
            name = ""
            if "given" in author_dict:
                name = author_dict["given"] + " "
            if "family" in author_dict:
                name += author_dict["family"]
            a = Person(id=BNode(), name=name)
            if "sequence" in author_dict:
                s = author_dict["sequence"]
                if s == "first":
                    first_author = a
                else:
                    authors.append(a)
            else:
                authors.append(a)
        if first_author:
            authors.insert(0, first_author)
        pub.author = authors
    if "published" in data:
        published = data["published"]
        if "date-parts" in published:
            date_published = "-".join(
                [str(i) for i in published["date-parts"][0]]
            )
            pub.datePublished = date_published
    return pub
