"""
Functions to map OGC CSW record elements first via GeoDCATAP to the
the NFDI4Earth KH metadatada schema
"""

# TODOs:
# - implement postprocessing -> gemet keys to URIs
# better solution for conformsTo

import logging
from typing import Dict, Tuple, Optional
import requests
import shapely.wkt
from shapely.geometry.polygon import orient
from lxml import etree as ET
from io import BytesIO
from rdflib import Graph, URIRef, BNode, Literal
from rdflib.namespace import RDF, XSD
from kh_populator_logic.validity_tests import validate_period_of_time

from n4e_kh_schema_py.n4eschema import Dataset, DataService, Location
from kh_populator_logic.rdf import (
    rdf_graph_to_metadata_object,
    get_gemet_concept_for_keyword,
    DCAT,
    SKOS,
    DCT,
    SCHEMA,
    FOAF,
    GEO,
)

ISO_SCHEMA_URL = "http://www.isotc211.org/2005/gmd"
xslURL = (
    "https://raw.githubusercontent.com/SEMICeu/"
    + "iso-19139-to-dcat-ap/master/iso-19139-to-dcat-ap.xsl"
)
xslTransform = None

GEMET_MATCH_BASE_URL = (
    "https://www.eionet.europa.eu/gemet/getConceptsMatchingKeyword"
)
gemet_conecepts_cache: Dict[Tuple, URIRef] = {}

log = logging.getLogger(__name__)


def match_gemet_concept(keyword: str, lang: str = "") -> Optional[URIRef]:
    if (keyword, lang) in gemet_conecepts_cache:
        return gemet_conecepts_cache[(keyword, lang)]
    concept_uri = get_gemet_concept_for_keyword(keyword, lang)
    if concept_uri:
        gemet_conecepts_cache[(keyword, lang)] = concept_uri
        return concept_uri


class DTDResolver(ET.Resolver):
    def resolve(self, url, id, context):
        return self.resolve_empty(context)


def init_xslTransform():
    global xslTransform
    parser = ET.XMLParser()
    parser.resolvers.add(DTDResolver())
    xslTransform = ET.XSLT(
        ET.parse(BytesIO(bytes(requests.get(xslURL).text, "utf-8")), parser)
    )


def csw_record_to_n4e(record):
    if xslTransform is None:
        init_xslTransform()

    # ugly fix to handle WDCC ISO-GMD metadata collection files which currently
    # encode "eng; USA" into <gmd:language>-><gco:CharacterString> which leads
    # all literals to be parsed as localized literals by the RDF xsltransform
    iso_metadata = ET.parse(
        BytesIO(
            ET.tostring(record.md).replace(
                bytes("eng; USA", encoding="utf-8"),
                bytes("en-USA", encoding="utf-8"),
            )
            # temporary fix for problematic DLR GMD serialization
            .replace(
                bytes(
                    ";httpAccept=application%2Fatom%2Bxml\n                ",
                    encoding="utf-8",
                ),
                bytes(
                    ";httpAccept=application%2Fatom%2Bxml\n", encoding="utf-8"
                ),
            )
        )
    )
    try:
        geodcat_rdf = ET.tostring(xslTransform(iso_metadata)).decode("utf-8")
    except ET.Error:
        log.warning(f"Error during XSLT transform {record.identification[0]}")
        return None
    g = Graph()
    g.parse(data=geodcat_rdf, format="xml")

    # post-processing step 1: on the serialized RDF graph
    # first: get the IRI of the principal resource
    if (
        record.hierarchy == "dataset"
        or record.hierarchy == "collection"
        or record.hierarchy == "series"
    ):
        resource_type = Dataset
    elif record.hierarchy == "service":
        resource_type = DataService
    subject = URIRef("")
    if record.hierarchy == "collection":
        identification = record.identification[0]
        subject_titles = g.subject_objects(predicate=DCT.title)
        for subject_, title in subject_titles:
            if (
                isinstance(title, Literal)
                and title.value == identification.title
            ):
                subject = subject_
        if not subject:
            raise ValueError("Did not find subject")
        g.add((subject, RDF.type, DCAT.Dataset))
    else:
        subject = list(
            g.subjects(
                predicate=RDF.type, object=resource_type.class_class_uri
            )
        )[0]

    # 1.1 handle theme
    themes_iter = g.objects(subject=subject, predicate=DCAT.theme)
    remove_themes = []
    for theme in themes_iter:
        # NOTE: When the theme is already provided with an IRI (= a reference
        # to an entry of a controlled vocabulary) then we take it into the KH
        # as it is, because currently we do not enforce a certain vocabulary.
        # NOTE2: Currently (as of Sep 2024) we are observing that some GDI DE
        # metadata entries reference the GEMET concept, but using not the
        # correct IRIs, we see for example
        # https://www.eionet.europa.eu/gemet/en/concept/6760
        # where it should be https://www.eionet.europa.eu/gemet/concept/6760
        # The incorrect references are imported into the KH currently, however
        # this problem should be tackled by changing it at the source (on the
        # side of the GDI-DE).
        if isinstance(theme, BNode):
            remove_themes.append(theme)
            scheme = list(g.objects(subject=theme, predicate=SKOS.inScheme))
            if len(scheme) == 1:
                conceptLabels = list(
                    g.objects(subject=theme, predicate=SKOS.prefLabel)
                )
                if len(conceptLabels) == 1 and isinstance(
                    conceptLabels[0], Literal
                ):
                    conceptLabel = conceptLabels[0].value
                    lang = conceptLabels[0].language
                    gemet_uri = match_gemet_concept(conceptLabel, lang)
                    if gemet_uri:
                        g.add((subject, DCAT.theme, gemet_uri))
                    else:
                        g.add((subject, DCAT.keyword, conceptLabels[0]))
    for theme in remove_themes:
        g.remove((subject, DCAT.theme, theme))

    # 1.2 handle description and name properties
    # (other namespace in our schema, same meaning, is an exact match)
    for subject_, name in g.subject_objects(predicate=FOAF.name):
        g.add((subject_, SCHEMA.name, name))
    for description in g.objects(subject=subject, predicate=DCT.description):
        g.add((subject, SCHEMA.description, description))

    # 1.3: linkml loading from the rdflib Graph fails with empty date fields
    # or also when there is a "Z" for UTC time, which is not understood
    # by the Python datetime library in general
    for subject_, predicate_, object_ in g.triples((None, None, None)):
        if isinstance(object_, Literal) and object_.datatype in [
            XSD.date,
            XSD.dateTime,
        ]:
            if len(object_) == 0:
                # remove empty datetime statements
                log.warning(f"Unable to parse modified field on {subject_}")
                g.remove((subject_, predicate_, object_))
            elif "Z" in object_:
                g.remove((subject_, predicate_, object_))
                if len(object_) == 11:
                    datatype = XSD.date
                else:
                    datatype = object_.datatype
                g.add(
                    (
                        subject_,
                        predicate_,
                        Literal(object_.replace("Z", ""), datatype=datatype),
                    )
                )

    # 1.4: the ISO19139-to-GeoDCAT transformer might endup setting
    # :s dct:spatial :a-remote-uri, however, our schema expects a blank node
    # with the actual spatial coverage, therefore, remove URIRefs to remote
    # spatial coverages
    for subject_, object_ in g.subject_objects(predicate=DCT.spatial):
        if isinstance(object_, URIRef):
            g.remove((subject_, DCT.spatial, object_))

    # 1.5 handle conformsTo statement currently not matching KH schema
    g.remove((subject, DCT.conformsTo, None))
    # TODO: conformsTo can either point to a URIRef or a blanknode following
    # "is a dct:Standard" -> in both cases, try to match to existing
    # MetadataStandard in the KH, decide what to do if it fails...
    # possibly allow in KH schema that the value can also be a blank node,
    # align schemata for n4e:MetadataStandard and dct:Standard
    # De-serialize the actual Python object from the rdf graph
    resource = rdf_graph_to_metadata_object(
        g, resource_type, ignore_unmapped_predicates=True
    )

    # post-processing step 2: on the de-serialized Python object
    # 2.1 handle spatial extent
    # the iso-to-geodcat XSL generates the same bounding box several
    # times, as GeoJSON, WKT and GML
    # Make sure, that in the LinkML representation only the WKT
    # serialization is used
    bbox = ""
    for subject_, bbox_ in g.subject_objects(predicate=DCAT.bbox):
        if isinstance(bbox_, Literal) and bbox_.datatype == GEO.wktLiteral:
            bbox = bbox_
    if len(bbox) > 0:
        if resource.spatialCoverage:
            resource.spatialCoverage.boundingBox = bbox
        else:
            resource.spatialCoverage = Location(boundingBox=bbox)
    # if the XSL has generated the bounding box as geojson, remove this
    if (
        isinstance(resource.spatialCoverage.geometry, Literal)
        and resource.spatialCoverage.geometry.datatype != GEO.wktLiteral
    ):
        resource.spatialCoverage.geometry = None
    # if an exacter geometry is not available but a bounding box, use the
    # bounding box polygon as geometry, so that the geometry field is
    # always filled (easier for processing)
    if (
        resource.spatialCoverage.geometry is None
        and resource.spatialCoverage.boundingBox
    ):
        resource.spatialCoverage.geometry = (
            resource.spatialCoverage.boundingBox
        )
    # verify that geometry is valid!
    # TODO: we should annotate/ store somewhere when we find issues with
    # the original data and make changes ...
    if resource.spatialCoverage.geometry:
        geom = shapely.wkt.loads(resource.spatialCoverage.geometry)
        if not geom.is_valid:
            resource.spatialCoverage.geometry = None
            print("Setting invalid geom to none!")
        if geom.geom_type == "Polygon":  # if Polygon, adjust orientation
            geom = orient(geom)
            resource.spatialCoverage.geometry = Literal(
                geom.wkt,
                datatype=URIRef(
                    "http://www.opengis.net/ont/geosparql#wktLiteral"
                ),
            )
    if resource.spatialCoverage.boundingBox:
        if not shapely.wkt.loads(
            resource.spatialCoverage.boundingBox
        ).is_valid:
            resource.spatialCoverage.boundingBox = None
            print("Setting invalid bbox to none!")

    # 2.2 add variableMeasured which is mapped by the
    # iso-to-dcat translator
    ns = {
        "gmd": "http://www.isotc211.org/2005/gmd",
        "gco": "http://www.isotc211.org/2005/gco",
    }
    for contentinfo_el in record.md.findall(
        "gmd:contentInfo",
        namespaces=ns,
    ):
        for coverage_description_md in contentinfo_el.findall(
            "gmd:MD_CoverageDescription",
            namespaces=ns,
        ):
            for attribute_description_md in coverage_description_md.findall(
                "gmd:attributeDescription",
                namespaces=ns,
            ):
                for record_type in attribute_description_md.findall(
                    "gco:RecordType",
                    namespaces=ns,
                ):
                    value = record_type.text
                    if value:
                        resource.variableMeasured.append(value)

    # experimental: better document the original data provider/ source:
    if (
        record.distribution
        and record.distribution.distributor
        and len(record.distribution.distributor) == 1
        and record.distribution.distributor[0].contact
        and record.distribution.distributor[0].contact.organization
    ):
        resource.originalDataSource = record.distribution.distributor[
            0
        ].contact.organization

    # 3 Validation
    # temporal coverage
    if (
        resource_type == Dataset
        and resource.temporal
        and not validate_period_of_time(resource.temporal)
    ):
        resource.temporal = []

    return resource
