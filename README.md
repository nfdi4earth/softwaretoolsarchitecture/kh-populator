# KH populator protoype

This repo provides a unified Python script for the ingestion of metadata and transformation to the metadata schema for the Knowledge Hub (KH) within [NFDI4Earth](https://nfdi4earth.de/).

For further documentation see also the [wiki](https://git.rwth-aachen.de/nfdi4earth/knowledgehub/kh-populator/-/wikis/home)

## Installation

Requires Python3.8 or higher. The suggested installation mode for development is:

```bash
make venv-init-dev
# if dev tools are not needed, then
make venv-init
# (it is important that the virtual environment is created in venv directory (.) and not anywhere else)
source venv/bin/activate
```

Note: We add the Knowledge Hub schema dependency from source, which cannot be configured in `setup.py`, therefore in the `Makefile` script you find the additional dependency in `requirements.txt`:

There is a slight modification to the linkml_runtime package required, which handles the way how objects in Python are serialized to and de-serialized from RDF. The patch configuration is found in the `venv-init` script in the `Makefile`.

NOTE: This package requires the `harvesters-config.json` file. You can find it in the folder [kh_populator](./kh_populator/), [here](./kh_populator/harvesters-config.json). You will also need to add an entry for a new system when implementing a new harvester.

## Use

`kh-populator` makes use of [celery](https://docs.celeryq.dev/en/stable/#) for queuing and scheduling of the individual harvesting tasks. To run celery, a message broker is required. We recommend to use [RabbitMQ](https://www.rabbitmq.com/).

Therefore, to start the harvesting setup:

1. Run the message broker required by celery:
   `docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 -e RABBITMQ_DEFAULT_USER=mquser -e RABBITMQ_DEFAULT_PASS=mqpassword rabbitmq:3.12-management-alpine`

2. In a new tab, from the root of this project go into the folder `kh_populator` and run celery:
   `celery -A celery_deploy worker --loglevel=INFO -E -Q ROR,RDAMSC,ORCID,WIKIDATA,RE3DATA,LHB,RSD1,RSD2,ISOGMD,ZENODO,DLRSTAC,DTHB,celery -c 4`

3. In another new tab, move back to the project root (`cd ..`) and call the search and harvest job for an individual pipeline, e.g.,
   `kh_populator populate-kh -p search-and-update-lhb`

Options:

- `--pipeline, -p`: Specify the harvesting pipeline to run

**NOTE**: The commands above assume per default a locally running [setup of the Knowledge Hub](https://git.rwth-aachen.de/nfdi4earth/knowledgehub/knowledge-hub-backend-setup). However, for testing purposes it is also possible to harvest into a file saved locally on dist in RDF format (in [TriG Syntax](https://www.w3.org/TR/trig/)). For this, change the configuration in `kh_populator/config.ini` and set `[celery].target` parameter to `local_rdf_file` (and possibly also change the path value of `[rdf].filename`).

When using `local_rdf_file` you should run celery only with one worker, e.g. `celery -A celery_deploy worker --loglevel=INFO -E -Q ROR,celery -c 1`

### Configuration

Further configuration, for example whether the harvesting should be stored in a local test KH instance or in the production KH instance is defined in the `config.ini` file.

## Metadata schema

During harvesting, all remote metadata are mapped to the metadata schema of the Knowledge Hub. The schema is being developed in a separate repo. You can find the documentation of this schema here: https://nfdi4earth.pages.rwth-aachen.de/knowledgehub/nfdi4earth-kh-schema/.

Further information on the underlying modelling framework (LinkML) and how the Python classes which represent the schema are generated, can be found here: https://git.rwth-aachen.de/nfdi4earth/knowledgehub/nfdi4earth-kh-schema/-/blob/main/README.md. The Python classes are imported via the dependency in `requirements.txt`.

## For developers

### Development dependencies

```bash
pip install -r dev-requirements.txt
```

### Code structure

This package contains subpackages. The most important structure is:

- `kh_populator` is the main package which contains the code which triggers harvesting pipelines
- `kh_populator_domain` contains modules with domain specific function for the harvesting and transformation of external (meta)data sources - these functions should be called from the respective pipeline
- `kh_populator_logic` contains useful functions which might be required in different domain modules

### Run tests

```bash
pytest tests/
```

### Recommendation

All network communication with the remote database which is harvested as well as with the KH should happen in the submodules of `kh_populator`. In contrast, the mapping of an external metadata format to the KH format and schema should happen via a function in `kh_populator_domain` without any external calls. Therefore it is recommended to write a unit test for the respective transformation/mapping function, see for an example the module `tests/domain/gitlab_source.py` and the belonging data file `tests/data/lhb-article.md.

It is recommended to create such a unit test and then during development invoke it, e.g. via

`pytest tests/domain/gitlab_source.py -vv` as this is faster than running the asynchronous harvesting via celery each time.

### How to add a harvester pipeline

1. For each remote database which should be harvested into the KH, we create a _searcher_ in `kh_populator/harvesting_searchers/` and a _consumer_ in `kh_populator/harvesting_consumers/`. The _searcher_ is responsible for searching the remote database for resource objects which should be added into the KH. For each found resource object, the corresponding task is added to a celery queue. Then, the _consumer_ is responsible for taking up each task in the queue.

2. Add a user account for the new harvester in `kh_populator/harvesters-config.json`.

3. Add the new harvester the `Sources` enum and source systems list in `kh_populator/util.py`

4. Include the harvester in the celery config (and set a cron schedule) in `kh_populator/celery_deploy.py`

5. Restart celery and include the pipeline queue in the command, e.g. `celery -A celery_deploy worker --loglevel=INFO -E -Q ROR,NEWSOURCE-c 4`

### Install development version of KH Schema

Only if you need to map properties and classes which are not yet included in the [KH metadata schema](https://nfdi4earth.pages.rwth-aachen.de/knowledgehub/nfdi4earth-kh-schema/) then you need to clone that project locally and add the slots/classes by yourself:

```
# in a separate location on your filesystem
git clone git@git.rwth-aachen.de:nfdi4earth/knowledgehub/nfdi4earth-kh-schema.git
cd nfdi4earth-kh-schema/
# The following command initializes y Python virtual env and installs the package
make venv-init
# make changes to the schema in n4eschema-linkml.yaml
# afterwards call at least:
source venv/bin/activate && make generate-python
# or
source venv/bin/activate && make dist
```

In order to use this within `kh-populator` move to the root of this repository **and make sure that the correct venv is activated (kh-populator/venv)**. Then, uninstall the remote `nfdi4earth_kh_schema` and install from your local fork:

```bash
pip uninstall nfdi4earth_kh_schema
# The following depends on the location
pip install -e ../nfdi4earth-kh-schema   # You will have to 1) clone that repo (`nfdi4earth-kh-schema`) from the 4Earth GitLab if you do not have it yet, and 2) build it from the main branch, as described in the README file in that repostitory.
```

Or install from a locally built wheel:

```bash
pip install ../nfdi4earth-kh-schema/dist/nfdi4earth_kh_schema-*.whl
```

### Debugging in VS Code

The following launch configuration runs a particular pipeline or a specific test in debug mode in Visual Studio Code:

```json
{
    "name": "KH Populator: RSD",
    "type": "python",
    "request": "launch",
    "stopOnEntry": false,
    "program": "${workspaceRoot}/kh_populator/cli.py", // see https://stackoverflow.com/a/64558717
    "console": "internalConsole", // so that active venv is used
    "justMyCode": true,
    "args": [
        "--debug",
        "populate-kh",
        "-p", "harvest_rsd",
        "-t", "stdout",
    ],
},
{
    "name": "Pytest kh_populator",
    "type": "python",
    "request": "launch",
    "stopOnEntry": false,
    "module": "pytest",
    "console": "internalConsole", // so that active venv is used
    "justMyCode": true,
    "args": [
        "tests/test_harvest_rsd.py",
    ],
}
```

## License

This project is published under the Apache License 2.0, see file `LICENSE`.

Contributors: Jonas Grieb, Ralf Klammer, Daniel Nüst, Christopher Purr, Johannes Munke, Alexander Wellmann, Tim Schäfer
